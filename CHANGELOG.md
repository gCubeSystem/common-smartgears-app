# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v4.0.0] 

- moved to jakarta and servelt6

## [v3.0.0] - 2022-05-13

- porting to new IAM

## [v2.1.0] - 2022-02-04

- added OfflineObserver for test purpose

## [v2.0.3] - 2020-11-03

- removed WARNING log on Vfs.Dir at startup (https://support.d4science.org/issues/18551)
