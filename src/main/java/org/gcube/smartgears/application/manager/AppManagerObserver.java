package org.gcube.smartgears.application.manager;

import java.util.Set;

import org.gcube.common.security.factories.AuthorizationProvider;
import org.gcube.smartgears.ApplicationManager;
import org.gcube.smartgears.context.application.ApplicationContext;

public interface AppManagerObserver {

	void onRegistration(String parameter);

	void onRemove(String securityToken);

	void onStop(ApplicationContext appContext);

	void unregister();
	
	public void setAuthorizationProvider(AuthorizationProvider authProvider);
	
	void setApplicationManagerClasses(Set<Class<? extends ApplicationManager>> managersClasses);
	
	public void register();

}