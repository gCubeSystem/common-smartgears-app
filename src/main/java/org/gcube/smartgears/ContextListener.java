package org.gcube.smartgears;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.gcube.smartgears.annotations.ManagedBy;
import org.gcube.smartgears.application.manager.AppManagerObserver;
import org.gcube.smartgears.context.application.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.github.classgraph.ClassGraph;
import io.github.classgraph.ClassInfo;
import io.github.classgraph.ClassInfoList;
import io.github.classgraph.ScanResult;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;

@WebListener
public class ContextListener implements ServletContextListener {

	private static Logger log = LoggerFactory.getLogger(ContextListener.class);
	
	AppManagerObserver observer;

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		
		ApplicationContext context = (ApplicationContext) sce.getServletContext().getAttribute(Constants.context_attribute);

		if (context==null) {
			String msg = sce.getServletContext().getContextPath()+" is a gCube-aware application but is not managed as a gCube resource: missing or invalid context attribute "+Constants.context_attribute;
			throw new RuntimeException(msg);
		} 
		
		log.info("configuring context provider for {}",context.name());
		ContextProvider.set(context);
		retrieveAndRegisterManagers(context);
				
	}


	private void retrieveAndRegisterManagers(ApplicationContext context) {
		ApplicationManagerProvider.init(context);
		
		Set<Class<?>> annotatedManaged;
		
		try (ScanResult result = new ClassGraph().enableClassInfo().enableAnnotationInfo().scan()) {

			ClassInfoList classInfos = result.getClassesWithAnnotation(ManagedBy.class.getName());

			annotatedManaged = classInfos.stream().map(ClassInfo::loadClass)
					.collect(Collectors.toSet());
		}
		/*
		Collection<URL> urls =  ClasspathHelper.forJavaClassPath();
		urls.removeIf(url ->  url.toString().endsWith(".so") || url.toString().endsWith(".zip")  );
		
		
		ConfigurationBuilder reflectionConf =  new ConfigurationBuilder().addUrls(urls).addClassLoaders(Thread.currentThread().getContextClassLoader()).setScanners(new TypeAnnotationsScanner(), new SubTypesScanner());
		
		Reflections reflection = new Reflections(reflectionConf); 
		
		Set<Class<?>> toInitialize = reflection.getTypesAnnotatedWith(ManagedBy.class);
		*/
		log.debug("annotated managed are {} for {}", annotatedManaged.size(),context.name());
		Set<Class<? extends ApplicationManager>> managers = new HashSet<Class<? extends ApplicationManager>>();
		for (Class<?> initializer: annotatedManaged ){
			ManagedBy manageBy = initializer.getAnnotation(ManagedBy.class);
			log.info("ApplicationManager added {} to {} @ {}", manageBy.value().getSimpleName(), initializer.getSimpleName(), context.name());
			managers.add(manageBy.value());
		}
		if (managers.size()>0){
			observer = ApplicationManagerProvider.instance.getObserver();
			observer.setAuthorizationProvider(context.container().authorizationProvider()); 
			observer.setApplicationManagerClasses(managers);
			observer.register();
			context.events().subscribe(observer);
		}
		else {
			log.info("no application managers found for {}",context.name());
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		if (observer!=null){
			ApplicationContext context = (ApplicationContext) sce.getServletContext().getAttribute(Constants.context_attribute);
			context.events().unsubscribe(observer);
			observer.onStop(context);
		}
	}

}
